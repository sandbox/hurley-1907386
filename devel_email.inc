<?php
/**
 * @file
 * Devel Email settings form.
 * Created by: Rob Barnett, Babson College 2012.
 */

/**
 * Settings form.
 */
function devel_email_settings() {
  $form[DEVEL_EMAIL_ENABLE] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Divert emails to watchdog'),
    '#default_value' => variable_get(DEVEL_EMAIL_ENABLE, 0),
    '#description'   => t('Check this box to divert email to watchdog. Uncheck to turn back on email sending.'),
  );

  return system_settings_form($form);
}
