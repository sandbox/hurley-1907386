README.txt
---------------
The Devel Email module is a tool for developers
which diverts all emails to watchdog.  It is meant
to only be run in development environments much
the way that the devel module is used in Drupal 6.x.
I created this module after noticing that devel
removed the smtp log functionality in its 7.x version.

INSTALLATION
--------------
Follow the instructions in the Drupal contributed
module installation guide:
http://drupal.org/documentation/install/modules-themes/modules-7

CONFIGURATION
--------------
After installing and enabling the module, go to
admin/config/development/devel_email.
Check off Divert emails to watchdog and save configuration.

USAGE
--------------
After saving the Devel Email configuration, check watchdog
for emails diverted to log: admin/reports/dblog
You can filter by devel_email.  The email log purposely
displays the email message results in a print_r format for
debugging.  Click on the Array... link to view the email message.
